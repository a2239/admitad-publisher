package com.panferov.admitadpublisher.service.schedulers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UpdateProcessLauncher {
    private final PartnerScheduler partnerScheduler;
    private final CouponScheduler couponScheduler;

    @Async
    // @Scheduled(cron = "* */1 * * * *")
    @Scheduled(cron = "0 0/58 1 * * *")
    public void update() {
        log.info("Update had been start");
        partnerScheduler.run();
        couponScheduler.run();
    }
}
