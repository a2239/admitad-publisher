package com.panferov.admitadpublisher.service.sender;

import java.util.List;

public interface RabbitMQSender<T> {

    List<T> send(List<T> batch);
}
