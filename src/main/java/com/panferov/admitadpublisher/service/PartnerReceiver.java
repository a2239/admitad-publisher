package com.panferov.admitadpublisher.service;

import com.panferov.admitadpublisher.client.AdmitadContentClient;
import com.panferov.admitadpublisher.config.AdmitadProperties;
import com.panferov.admitadpublisher.model.admitad.PartnerDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
@AllArgsConstructor
public class PartnerReceiver {

    private final WebmasterWebsiteService webmasterWebsiteService;
    private final AdmitadContentClient admitadContentClient;

    private final AdmitadProperties admitadProperties;

    public List<PartnerDto> getPartners() {
        log.info("Receive partners from admitad");
        return admitadContentClient.partnerForSite(webmasterWebsiteService.getWebsiteId(admitadProperties.getAreaName()))
                .getPartners();
    }
}
